# Perkable redemption smart contract


### File ABI:
`/abis/Redemption.json`

### Contract address: 
- polygon test (80001): `0x7521eff52e75839a8965d1453073395D56d64862`
- polygon mainnet (137): `0x51DcD66190F2Cef869D2d90837Eb676CA9b6F684`

### Usage:

Copy folder abis to your project

```javascript
// Create a new instance of the contract
import {abi} from "./Redemption.json";

const contract = new ethers.Contract(address, abi, signer);

// Create redemption ( mint and transfer to minter)
await contract.mint(redemptionId, limit);

// Transfer redemption to users
const users = ['0xAecF...', '0xF3cF...'];
await contract.transferBulk(users, redemptionId);

// Burn NFT
await contract.burn(await signer.getAddress(), redemptionId, amount);

// Check balance
const balance = await contract.balanceOf(userAddress, redemptionId)

// Get redemption metadata link
const metadata = await contract.uri(redemptionId)

// Put redemption to sale
await contract.setApprovalForAll(contract.address, true);
await contract.putOnSale(redemptionId, amount, ethers.utils.parseEther("1"));

// Take redemption off sale
await contract.takeOffSale(redemptionId);

// Buy redemption
await contract.buy(redemptionId, sellerAddress, amount, {
    value: ethers.utils.parseEther("1")
});
```

