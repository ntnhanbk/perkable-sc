const hre = require("hardhat");
const {ethers, upgrades} = require("hardhat");

async function main() {
    const contractFactory = await ethers.getContractFactory('Redemption');
    const contract = await upgrades.deployProxy(contractFactory, [process.env.API], {initializer: 'initialize'});
    await contract.deployed();

    console.log("Proxy deployed to: ", contract.address);

    console.log(await upgrades.erc1967.getImplementationAddress(contract.address)," getImplementationAddress")
    console.log(await upgrades.erc1967.getAdminAddress(contract.address), " getAdminAddress")
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
