//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC1155/ERC1155Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/extensions/ERC1155BurnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC1155/extensions/ERC1155PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlEnumerableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts/utils/Strings.sol";

contract Redemption is Initializable, ContextUpgradeable, AccessControlEnumerableUpgradeable, ERC1155BurnableUpgradeable, ERC1155PausableUpgradeable {
    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");

    mapping(address => mapping(uint256 => uint256)) public sellerToIdToPrice;
    mapping(address => mapping(uint256 => uint256)) public sellerToIdToAmount;

    function initialize(string memory uri_) public virtual initializer {
        __ERC1155PresetMinterPauser_init(uri_);
    }

    /**
    * @dev Grants `DEFAULT_ADMIN_ROLE`, and `PAUSER_ROLE` to the account that
    * deploys the contract.
    */
    function __ERC1155PresetMinterPauser_init(string memory uri_) internal onlyInitializing {
        __ERC1155_init_unchained(uri_);
        __Pausable_init_unchained();
        __ERC1155PresetMinterPauser_init_unchained(uri_);
    }

    function __ERC1155PresetMinterPauser_init_unchained(string memory) internal onlyInitializing {
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());

        _setupRole(PAUSER_ROLE, _msgSender());
    }

    /**
    * @dev Creates `amount` new tokens for `to`, of token type `id`.
    *
    * See {ERC1155-_mint}.
    *
    */
    function mint(
        uint256 id,
        uint256 amount
    ) public {
        _mint(msg.sender, id, amount, "");
    }

    function transferBulk(address[] calldata receivers, uint256 id) public {
        require(balanceOf(msg.sender, id) >= receivers.length, "Not enough tokens to transfer");
        for (uint256 i = 0; i < receivers.length; i++) {
            safeTransferFrom(msg.sender, receivers[i], id, 1, "");
        }
    }

    function putOnSale(uint256 id, uint256 amount, uint256 price) public {
        require(balanceOf(msg.sender, id) >= amount, "Not enough tokens to set price");
        require(isApprovedForAll(msg.sender, address(this)), "You must be approved for all");
        sellerToIdToPrice[msg.sender][id] = price;
        sellerToIdToAmount[msg.sender][id] = amount;
    }

    function takeOffSale(uint256 id) public {
        sellerToIdToPrice[msg.sender][id] = 0;
        sellerToIdToAmount[msg.sender][id] = 0;
    }

    function buy(uint256 id, address seller, uint amount) payable public {
        require(balanceOf(seller, id) >= 1, "Not enough tokens to set price");
        require(sellerToIdToAmount[seller][id] >= amount, "No tokens on sale");
        require(isApprovedForAll(seller, address(this)), "Seller not approve");
        require(msg.value == (sellerToIdToPrice[seller][id] * amount), "Price mismatch");

        sellerToIdToAmount[seller][id] -= amount;
        _safeTransferFrom(seller, msg.sender, id, amount, "");

        payable(seller).transfer(msg.value);
    }

    /**
     * @dev Pauses all token transfers.
    *
    * See {ERC1155Pausable} and {Pausable-_pause}.
    *
    * Requirements:
    *
    * - the caller must have the `PAUSER_ROLE`.
    */
    function pause() public virtual {
        require(hasRole(PAUSER_ROLE, _msgSender()), "ERC1155PresetMinterPauser: must have pauser role to pause");
        _pause();
    }

    /**
     * @dev Unpauses all token transfers.
     *
     * See {ERC1155Pausable} and {Pausable-_unpause}.
     *
     * Requirements:
     *
     * - the caller must have the `PAUSER_ROLE`.
     */
    function unpause() public virtual {
        require(hasRole(PAUSER_ROLE, _msgSender()), "ERC1155PresetMinterPauser: must have pauser role to unpause");
        _unpause();
    }

    function setURI(string memory _uri) public {
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "Must be admin to set URI");
        _setURI(_uri);
    }

    function uri(uint256 id) public view override(ERC1155Upgradeable) returns (string memory) {
        return string(abi.encodePacked(super.uri(id), Strings.toString(id), ".json"));
    }

    /**
     * @dev See {IERC165-supportsInterface}.
     */
    function supportsInterface(bytes4 interfaceId)
    public
    view
    virtual
    override(AccessControlEnumerableUpgradeable, ERC1155Upgradeable)
    returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function _beforeTokenTransfer(
        address operator,
        address from,
        address to,
        uint256[] memory ids,
        uint256[] memory amounts,
        bytes memory data
    ) internal virtual override(ERC1155Upgradeable, ERC1155PausableUpgradeable) {
        super._beforeTokenTransfer(operator, from, to, ids, amounts, data);
    }

    /**
     * @dev This empty reserved space is put in place to allow future versions to add new
     * variables without shifting down storage in the inheritance chain.
     * See https://docs.openzeppelin.com/contracts/4.x/upgradeable#storage_gaps
     */
    uint256[50] private __gap;
}
