const { expect } = require("chai");
const { ethers, upgrades } = require("hardhat");

describe("Payment", function () {
  let contract;
  let deployer;
  let brand1;
  let brand2;
  let buyer1;
  let buyer2;

  beforeEach(async () => {
    [deployer, brand1, brand2, buyer1, buyer2] = await ethers.getSigners();

    const contractFactory = await ethers.getContractFactory("Redemption");
    contract = await contractFactory.deploy();

    contract = await upgrades.deployProxy(contractFactory, ["uri/"], {
      initializer: 'initialize'
    });
    await contract.deployed();
  });

  it("Should create redemption", async function () {
    const redeptionId = 1;
    const limit = 10;
    await contract.connect(brand1).mint(redeptionId, limit);

    expect(await contract.balanceOf(brand1.address, redeptionId)).to.equal(limit);
    expect(await contract.uri(redeptionId)).to.equal("uri/1.json");
  });

  it("Should transfer redemption", async function () {
    const redeptionId = 1;
    const limit = 10;
    const users = [buyer1.address, buyer2.address];
    await contract.connect(brand1).mint(redeptionId, limit);

    await contract.connect(brand1).transferBulk(users, redeptionId);
  });

  it("Should trade redemption", async function () {
    const redeptionId = 1;
    const limit = 10;
    const users = [buyer1.address, buyer2.address];
    await contract.connect(brand1).mint(redeptionId, limit);

    await contract.connect(brand1).transferBulk(users, redeptionId);

    // put on sale
    await contract.connect(buyer1).setApprovalForAll(contract.address, true);
    await contract.connect(buyer1).putOnSale(redeptionId, 1, ethers.utils.parseEther("1"));
    expect(await contract.sellerToIdToPrice(buyer1.address, redeptionId)).to.equal(ethers.utils.parseEther("1"));
    expect(await contract.sellerToIdToAmount(buyer1.address, redeptionId)).to.equal(1);

    // buy
    await contract.connect(buyer2).buy(redeptionId, buyer1.address, 1, {
        value: ethers.utils.parseEther("1")
    });
    expect(await contract.sellerToIdToAmount(buyer1.address, redeptionId)).to.equal(0);
    expect(await contract.balanceOf(buyer2.address, redeptionId)).to.equal(2);
  });

});
